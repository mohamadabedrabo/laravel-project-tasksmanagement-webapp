<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TasksController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Route::resource('tasks', 'App\Http\Controllers\TasksController')->middleware('auth');
Route::get('tasks/delete/{id}', 'App\Http\Controllers\TasksController@destroy')->name('tasks.delete');
Route::get('tasks/changeuser/{tid}/{uid?}', 'App\Http\Controllers\TasksController@changeUser')->name('tasks.changeuser');
Route::get('mytasks', 'App\Http\Controllers\TasksController@myTasks')->name('tasks.mytasks')->middleware('auth');
Route::get('thismonth', 'App\Http\Controllers\TasksController@thismonth')->name('tasks.thismonth')->middleware('auth');
Route::get('lastmonth', 'App\Http\Controllers\TasksController@lastMonth')->name('tasks.lastmonth')->middleware('auth');
Route::get("search","App\Http\Controllers\UsersController@Search");


Route::resource('users', 'App\Http\Controllers\UsersController')->middleware('auth');
Route::get('users/delete/{id}', 'App\Http\Controllers\UsersController@destroy')->name('users.delete');




Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
