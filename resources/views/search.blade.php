@extends('layouts.app')

@section('title', 'Users')

@section('content')
<div class="container">
	<h1>Full Text Search in Laravel 5.5 Example</h1>

	<form method="GET" action="{{ url('search') }}">
	  <div class="row">
		<div class="col-md-11">
		  <input type="text" name="search" class="form-control" placeholder="Full Search" value="{{ old('search') }}">
		</div>
		<div class="col-md-1">
		  <button class="btn btn-success">Search</button>
		</div>
	  </div>
	</form>

	<table class="table table-bordered">
	  <tr>
		<th>Id</th>
		<th>Name</th>
		<th>Email</th>
	  </tr>
	  @if($users->count())
		@foreach($users as $user)
		<tr>
		  <td>{{ $user->id }}</td>
		  <td>{{ $user->name }}</td>
		  <td>{{ $user->email }}</td>
		</tr>
		@endforeach
	  @else
	    <tr> 
		  <td colspan="3">Result not found.</td>
	    </tr>
	  @endif
	  <tr>
		<td colspan="3">{{ $users->links() }}</td>
	  </tr>
	</table>
  </div>
@endsection