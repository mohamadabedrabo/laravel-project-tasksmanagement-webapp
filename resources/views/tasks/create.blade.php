@extends('layouts.app')

@section('title', 'Create Task')

@section('content')
    <div class="title m-b-md">
    <h1>Create task</h1>
        <form method = "post" id = "createtask" action = "{{action('App\Http\Controllers\TasksController@store')}}">
        @csrf 
        <div class="form-group">
            <label for = "customer_name">customer name</label>
            <input type = "text" class="form-control" name = "customer_name">
        </div>   
            <label for = "description">description</label>
            <textarea form="createtask" name="description" class="form-control" rows="4" cols="50">
            </textarea>   
        <div class="form-group">
            <label for = "contract_duration">contract duration</label>
            <input type = "text" class="form-control" name = "contract_duration">
        </div>    
        <label for = "links">links</label>
            <textarea form="createtask" name="links" class="form-control" rows="4" cols="50">
            </textarea>   
        <div class="form-group">
            <label for = "outputs">outputs</label>
            <input type = "number" class="form-control" name = "outputs">
        </div>    
        <div class="form-group">
            <label for = "total_to_pay">total to pay</label>
            <input type = "number" class="form-control" name = "total_to_pay">
        </div>  
        <div class="form-group">
            <label for = "payed">payed</label>
            <input type = "number" class="form-control" name = "payed">
        </div>    
        <div class="form-group">
            <label for = "left_to_pay">left to pay</label>
            <input type = "number" class="form-control" name = "left_to_pay">
        </div>    
        <div class="form-group">
            <label for = "start_date">start date</label>
            <input type = "date" class="form-control" name = "start_date">
        </div>    
        <div class="form-group">
            <label for = "end_date">end date</label>
            <input type = "date" class="form-control" name = "end_date">
        </div>    
        <div>
            <input type = "submit" name = "submit" value = "Create task">
        </div>                       
        </form>    
    </div>
@endsection

