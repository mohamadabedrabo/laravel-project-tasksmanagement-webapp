@extends('layouts.app')

@section('title', 'Tasks')

@section('content')
    <div class="title m-b-md">
        <div><a href =  "{{url('/tasks/create')}}"> Add new task</a></div>
        <h1>My tasks</h1>
        <table class = "table table-dark">
            <tr>
                <th>Sum</th><th></th><th></th><th></th><th></th><th></th>
                <th>{{App\Http\Controllers\TasksController::sumOutputsMy()}}</th>
                <th>{{App\Http\Controllers\TasksController::sumTotalToPayMy()}}</th>
                <th>{{App\Http\Controllers\TasksController::sumPayedMy()}}</th>
                <th>{{App\Http\Controllers\TasksController::sumLeftToPayMy()}}</th>
                <th></th><th></th><th></th><th></th>
            </tr>
            <tr>
                <th>Id</th><th>Owner</th><th>Customer</th><th>Decription</th><th>Contract duration</th><th>Links</th><th>Outputs</th><th>Total to pay</th><th>Payed</th><th>Left to pay</th><th>Start date</th><th>End date</th><th>Edit</th><th>Delete</th>
            </tr>
            <!-- the table data -->
            @foreach($tasks as $task)
            <tr>
                <td>{{$task->id}}</td>
                <td>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if(isset($task->user_id))
                          {{$task->owner->name}}  
                        @else
                          Owner
                        @endif
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach($users as $user)
                      <a class="dropdown-item" href="{{route('tasks.changeuser',[$task->id,$user->id])}}">{{$user->name}}</a>
                    @endforeach
                    </div>
                  </div>                
                </td>
                <td>{{$task->customer_name}}</td>
                <td>{{$task->description}}</td>
                <td>{{$task->contract_duration}}</td>
                <td>{{$task->links}}</td>
                <td>{{$task->outputs}}</td>
                <td>{{$task->total_to_pay}}</td>    
                <td>{{$task->payed}}</td> 
                <td>{{$task->left_to_pay}}</td> 
                <td>{{$task->start_date}}</td>  
                <td>{{$task->end_date}}</td>  
                <td>
                    <a href = "{{route('tasks.edit',$task->id)}}">Edit</a>
                </td>      
                <td>
                    <a href = "{{route('tasks.delete',$task->id)}}">delete</a>
                </td>                              
            </tr>
            
            @endforeach
        </table>
    </div>
@endsection


