@extends('layouts.app')

@section('title', 'Edit task')

@section('content')
    <div class="title m-b-md">
    <h1>Edit task</h1>
        <form method = "post" id="edittask" action = "{{action('App\Http\Controllers\TasksController@update',$task->id)}}">
        @csrf
        @METHOD('PATCH')
        <div class="form-group">
            <label for = "customer_name">customer name</label>
            <input type = "text" class="form-control" name = "customer_name" value = {{$task->customer_name}}>
        </div>  
        <div class="form-group">
            <label for = "description">description</label>
            <textarea form="edittask" name="description" class="form-control" rows="4" cols="50" >
            {{$task->description}}
            </textarea>
        </div>      
        <div class="form-group">
            <label for = "contract_duration">contract duration</label>
            <input type = "text" class="form-control" name = "contract_duration" value = {{$task->contract_duration}}>
        </div>  
        <div class="form-group">
            <label for = "links">links</label>
            <textarea form="edittask" name="links" class="form-control" rows="4" cols="50" >
            {{$task->links}}
            </textarea>
        </div>     
        <div class="form-group">
            <label for = "outputs">outputs</label>
            <input type = "number" class="form-control" name = "outputs" value = {{$task->outputs}}>
        </div>    
        <div class="form-group">
            <label for = "total_to_pay">total to pay</label>
            <input type = "number" class="form-control" name = "total_to_pay" value = {{$task->total_to_pay}}>
        </div>  
        <div class="form-group">
            <label for = "payed">payed</label>
            <input type = "number" class="form-control" name = "payed" value = {{$task->payed}}>
        </div>    
        <div class="form-group">
            <label for = "left_to_pay">left to pay</label>
            <input type = "number" class="form-control" name = "left_to_pay" value = {{$task->left_to_pay}}>
        </div>    
        <div class="form-group">
            <label for = "start_date">start date</label>
            <input type = "date" class="form-control" name = "start_date" value = {{$task->start_date}}>
        </div>    
        <div class="form-group">
            <label for = "end_date">end date</label>
            <input type = "date" class="form-control" name = "end_date" value = {{$task->end_date}}>
        </div>    
        <div>
            <input type = "submit" name = "submit" value = "Update task">
        </div>                       
        </form>    
    </div>
@endsection