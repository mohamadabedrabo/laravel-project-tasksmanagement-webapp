@extends('layouts.app')

@section('title', 'Users')

@section('content')
    <div class="title m-b-md">
        <div><a class="nav-link" href="{{ route('register') }}"> Add new user</a></div>
        <h1>List of users</h1>
        <table class = "table table-dark">
            <tr>
                <th>id</th><th>name</th><th>email</th><th>created at</th><th>updated at</th><th>delete</th>
            </tr>
            <!-- the table data -->
            @foreach($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->created_at}}</td>
                <td>{{$user->updated_at}}</td>   
                <td>
                    <a href = "{{route('users.delete',$user->id)}}">delete</a>
                </td>                            
            </tr>
            @endforeach
        </table>
    </div>
@endsection


