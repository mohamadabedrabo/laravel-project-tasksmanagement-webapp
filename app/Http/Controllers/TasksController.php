<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Task;
use App\Models\Role;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $tasks = Task::all()->sortByDesc('start_date');
        return view('tasks.index', compact('tasks','users'));
    }

    public function thismonth()
    {
        $users = User::all();
        $tasks = Task::whereMonth('start_date', Carbon::now()->month)->get()->sortByDesc('start_date');
        return view('tasks.thismonth', compact('tasks','users'));
    }

    

    public function lastmonth()
    {        
        $users = User::all();
        $tasks = Task::whereMonth('start_date', (Carbon::now()->month)-1)->get()->sortByDesc('start_date');
        return view('tasks.lastmonth', compact('tasks','users'));
    }

    public function myTasks()
    {        
        $userId = Auth::id();
        $user = User::findOrFail($userId);
        $tasks = $user->tasks->sortByDesc('start_date');
        $users = User::all();
        return view('tasks.mytasks', compact('tasks','users'));
    }


    public function changeUser($tid, $uid = null){
        //Gate::authorize('assign-user');
        $task = Task::findOrFail($tid);
        $task->user_id = $uid;
        $task->save(); 
        return back();
    }


    public static function sumOutputs(){
        return(Task::whereMonth('start_date', Carbon::now()->month)->sum('outputs'));
    }

    public static function sumOutputsAll(){
        return(Task::all()->sum('outputs'));
    }

    public static function sumOutputsLast(){
        return(Task::whereMonth('start_date', (Carbon::now()->month)-1)->sum('outputs'));
    }

    public static function sumOutputsMy(){
        $userId = Auth::id();
        return(Task::where('user_id', $userId)->sum('outputs'));
    }

    public static function sumPayed(){
        return(Task::whereMonth('start_date', Carbon::now()->month)->sum('payed'));
    }

    public static function sumPayedAll(){
        return(Task::all()->sum('payed'));
    }

    public static function sumPayedLast(){
        return(Task::whereMonth('start_date', (Carbon::now()->month)-1)->sum('payed'));
    }

    public static function sumPayedMy(){
        $userId = Auth::id();
        return(Task::where('user_id', $userId)->sum('payed'));
    }

    public static function sumTotalToPay(){
        return(Task::whereMonth('start_date', Carbon::now()->month)->sum('total_to_pay'));
    }

    public static function sumTotalToPayAll(){
        return(Task::all()->sum('total_to_pay'));
    }

    public static function sumTotalToPayLast(){
        return(Task::whereMonth('end_date', (Carbon::now()->month)-1)->sum('total_to_pay'));
    }

    public static function sumTotalToPayMy(){
        $userId = Auth::id();
        return(Task::where('user_id', $userId)->sum('total_to_pay'));
    }

    public static function sumLeftToPay(){
        return(Task::whereMonth('start_date', Carbon::now()->month)->sum('left_to_pay'));
    }

    public static function sumLeftToPayAll(){
        return(Task::all()->sum('left_to_pay'));
    }

    public static function sumLeftToPayLast(){
        return(Task::whereMonth('start_date', (Carbon::now()->month)-1)->sum('left_to_pay'));
    }

    public static function sumLeftToPayMy(){
        $userId = Auth::id();
        return(Task::where('user_id', $userId)->sum('left_to_pay'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new Task();
        $tas = $task->create($request->all());
        $tas->save();
        return redirect('tasks');       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::findOrFail($id);
        return view('tasks.edit', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::findOrFail($id);
        $task->update($request->all());
        return redirect('tasks');  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete(); 
        return redirect('tasks'); 
    }
}
