<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    public $timestamps = false;
    
    protected $fillable = ['description','customer_name','contract_duration','links','outputs','total_to_pay','payed','left_to_pay','start_date','end_date'];
    use HasFactory;

    public function owner(){
        return $this->belongsTo('App\Models\User','user_id');
    }

}
