<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert([
                'description' => 'test 1 task',
                'contract_duration' => '3 months',
                'links' => 'https://www.liberaldictionary.com/wp-content/uploads/2018/11/task.png',
                'outputs' => 350,
                'total_to_pay' => 1000,
                'payed' => 700,
                'left_to_pay' => 300,
                'start_date' =>Carbon::create('2020', '09', '27'),
                'end_date' =>Carbon::create('2020', '10', '01')
            ]);  
    }
}
