<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            
                'name' => 'test customer 3',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
                             
            ]);         
    }
}